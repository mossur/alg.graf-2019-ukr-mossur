import java.io.File;
import java.util.Stack;
import java.util.Queue;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Collections;


public class graph
{
    private int V;
    private int[][] adj;
    int inf = 1000000000;

    public graph(int V) 
     {
        if(V<0) throw new IllegalArgumentException("More than 0");
        this.V=V;
        this.adj=new int[V][V];
        for(int i=0; i<V; i++) 
        {
            for(int j=0; j<V; j++) 
            {
                if(i==j) this.adj[i][j]=0;
                else this.adj[i][j]=inf;
            }
        }
     }
    private void validateVertex(int v)
    {
        if(v<0 || v>=V) throw new IllegalArgumentException("Wierzcholek"+v+"nie jest w przedziale [0, "+ (V-1)+"]" );
    }

    public void addEdge(int v, int w, int l)
    {
     validateVertex(v); validateVertex(w);
     adj[v][w] = l; 
     adj[w][v]=l;
    }
         
public void printGraphNum()
{
    System.out.println("Krawedzie grafu:");
    for(int row=0; row<V; ++row)
    {
        for(int col = 0; col <V; ++col)
        {
                if(adj[row][col]!=inf)
                {
                    System.out.print(row + "-");
                    System.out.print(col + "-"+adj[row][col]+" ");
                }
                else
                {
                    System.out.print(row + "-"+col+"-"+"inf ");
                }
        }
        System.out.println();
     }
     System.out.println();
}

public void showMatrix()
{
    for(int i =0; i<V; i++)
    {    
        for(int j =0; j<V; j++)
            {
                if(adj[i][j]!=inf)System.out.print(adj[i][j]+" ");
                else System.out.print("i ");
            }
            System.out.println();
    }           
}



void relax(int u, int v, int[] prev, int[] distance)
{
    if(distance[v]>distance[u]+adj[u][v])
    {
        distance[v] = distance[u]+adj[u][v];
        prev[v] = u;
    }
}

public void B_F(int src)
{
    int[] distance = new int[V];
    int[] prev = new int[V];
    for(int i=0; i<V; i++)
    {
        distance[i] = inf;
        prev[i] = 0;
    }
    distance[src] = 0;
    for(int i=0; i<(V-1); i++)
    {
        for(int u = 0; u<V; u++)
        {
            for(int v = 0; v<V; v++)
            {
                relax(u,v,prev,distance);
            }
        }
    }
    System.out.println("Distances from "+src+" to:");
    for(int i=0; i<V; i++)
        System.out.println("Vertex "+i+": "+distance[i]);
}



  public static void main(String[] args) throws Exception 
  {
      int V;
      V=6;
      graph G=new graph(V);
      G.addEdge(0,1,1);G.addEdge(0,5,4);G.addEdge(1,2,7);
      G.addEdge(1,5,5);G.addEdge(3,5,12);G.addEdge(4,5,4);
      G.addEdge(2,5,2);G.addEdge(2,4,3);G.addEdge(2,3,10);G.addEdge(3,4,6);
      G.printGraphNum();
      G.showMatrix();
      G.B_F(0);
      G.B_F(2);
  }
}
