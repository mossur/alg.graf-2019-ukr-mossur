import java.io.File;
import java.util.Stack;
import java.util.Queue;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Scanner;


public class graph 
{
    private int V;
    private boolean[][] adj;

    public graph(int V) 
     {
        if(V<0) throw new IllegalArgumentException("More than 0");
        this.V=V;
        this.adj=new boolean[V][V];
        for(int i=0; i<V; i++) 
        {
            for(int j=0; j<V; j++) 
            {
                this.adj[i][j]=false;
            }
        }
     }
   
    private void validateVertex(int v)
    {
        if(v<0 || v>=V) throw new IllegalArgumentException("Wierzcholek"+v+"nie jest w przedziale [0, "+ (V-1)+"]" );
    }
   
    public void addEdge(int v, int w)
    {
     validateVertex(v); validateVertex(w);
     adj[v][w] = true; 
     //adj[w][v]=true;
    }
   
   
public void printGraph()
{
    System.out.println("Krawedzie grafu:");
    for(int row=0; row<V; ++row) 
    {
        for(int col =0; col<V; ++col)
            {
                if(adj[row][col])
                    {
                        System.out.print((char)((int)'A'+row)+"-");
                        System.out.print((char)((int)'A'+col)+" ");
                    }
            }
        System.out.println();
    }
    System.out.println();
}
            
            
            
public void printGraphNum()
{
    System.out.println("Krawedzie grafu:");
    for(int row=0; row<V; ++row)
    {
        for(int col = 0; col <V; ++col)
        {
            if(adj[row][col])
            {
                System.out.print(row + "-");
                System.out.print(col + " ");
            }
        }
        System.out.println();
     }
     System.out.println();
}

public void readGraph(Scanner sc)
    {
    if(sc.hasNextInt())
        {
            int E = sc.nextInt();
            System.out.println("Krawedzi: "+E);
            for(int e=0; e<E; ++e)
            {
                int v=sc.nextInt();
                int w=sc.nextInt();
                this.addEdge(v,w);
            }
        }
    }



int getUnVisitedVertex(int a, boolean[] visited)
{
    for(int b=0; b<V; b=b+1)
    {
        if(adj[a][b]==true && visited[b]==false)return b;
    }
    return -1;
}

    
public void DFS(int a)
{
    boolean[] visited = new boolean[V];
    for(int k = 0; k<V; ++k) visited[k] = false;
    Stack<Integer> s = new Stack<Integer>();
    visited[a]=true;
    System.out.print(a+" ");
    s.push(a);
    while(!s.empty())
    {
        int b= getUnVisitedVertex(s.peek(),visited);
        if(b==-1) {s.pop();} else
        {
            visited[b]=true;
            System.out.print(b+" ");
            s.push(b);
        }
    }
    System.out.println();
}


public void BFS(int a)
{
    boolean[] visited = new boolean[V];
    Queue<Integer> q = new LinkedList<>();
    visited[a] = true;
    System.out.print(a + " ");
    q.add(a);
    while (q.size()!=0)
    {
        int b = q.peek();
        q.remove();
        int c;
        while ((c=getUnVisitedVertex(b, visited))!=-1)
        {
            visited[c] =true;
            System.out.print(c + " ");
            q.add(c);
        }
    }
    System.out.println();
}


public int max(int[] fin, int V)
{
    int tmp=0;
    for(int i=1; i<V; i++)
    {
        if(fin[i]>fin[tmp]) tmp=i;
    }
    return tmp;
}


public void t_DFS(int a)
{
    boolean[] visited = new boolean[V];
    int[] fin = new int[V];
    int[] time = new int[1];
    time[0]=0;
    for(int k = 0; k<V; ++k)
    {
        visited[k] = false;
        fin[k] = 0;
    }
    for(int k=0; k<V; ++k)
    {
        if(visited[k] == false) { 
          //System.out.println("Ide do t_visit z k=" + k);
          t_visit(k,time,visited,fin);
        }  
    }
    for(int k=0; k<V; ++k){
	   System.out.print(fin[k]+ " ");
	}
	System.out.println();
    
    for(int k=0; k<V; ++k)
    {
        int idx=max(fin,V);
        System.out.print(idx + " ");
        fin[idx] = -1;
    }
    System.out.println();
}


public void t_visit(int a, int[] time, boolean[] visited, int[] fin)
{
    System.out.println("wierz = " + a + "time:" + time[0]);
    time[0] = time[0]+1;
    visited[a] = true;
    int c = getUnVisitedVertex(a, visited);
    System.out.print("Nieodwiedzony: (" + a + "," + c + ")"); 
    while(c!=-1)
    {
        if(visited[c] == false){
            t_visit(c,time,visited,fin);
        }
        c = getUnVisitedVertex(a, visited);
    }
    time[0] = time[0]+1;
    fin[a] = time[0];
}
public void STBFS(int n)
{
    boolean[] visited = new boolean[V];
    for(int i = 0; i<V; i++)
    {
        visited[i] = false;
    }
    Queue<Integer> q = new LinkedList<>();
    visited[n] = true;
    q.add(n);
    while(q.size()!=0)
    {
        int b = q.peek();
        q.remove();
        int c;
        while((c=getUnVisitedVertex(b,visited)) != -1)
        {
            visited[c] = true;
            System.out.print(b + "-");
            System.out.print(c);
            q.add(c);
            System.out.print(" ");
        }
    }
    System.out.println();
}

  public static void main(String[] args) throws Exception 
  {
      int V;
      V=5;
      /*graph G=new graph(V);
      G.addEdge(0,1); G.addEdge(0,4);
      G.addEdge(1,2); G.addEdge(1,3);
      G.addEdge(1,4); G.addEdge(2,3);
      G.addEdge(3,4);  G.printGraph();
      */graph G2;     
      File file = new File(args[0]);
      Scanner sc=new Scanner(file);
      if(sc.hasNext())
      {
          V=sc.nextInt();
          System.out.println("Wierzcholkow: " + V);
          G2 = new graph(V);
          G2.readGraph(sc);
          G2.printGraphNum();
          G2.DFS(0);
         // G2.BFS(0);
          G2.t_DFS(0);
          System.out.println("Drzewo rozpinajace BFS");
          G2.STBFS(0);
      }     
      sc.close();
     //   System.out.println(G2.time[0]);
      
  } 
}  
