import java.io.File;
import java.util.Stack;
import java.util.Queue;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Scanner;


public class near_neigh 
{
    private int V;
    private int[][] adj;

    public near_neigh(int V) 
     {
        if(V<0) throw new IllegalArgumentException("More than 0");
        this.V=V;
        this.adj=new int[V][V];
        for(int i=0; i<V; i++) 
        {
            for(int j=0; j<V; j++) 
            {
                this.adj[i][j]=0;
            }
        }
     }
    private void validateVertex(int v)
    {
        if(v<0 || v>=V) throw new IllegalArgumentException("Wierzcholek"+v+"nie jest w przedziale [0, "+ (V-1)+"]" );
    }
    
    public void addEdge(int v, int w, int l)
    {
     validateVertex(v); validateVertex(w);
     adj[v][w] = l; 
     adj[w][v]=l;
    }
   
   
public void printGraph()
{
    System.out.println("Krawedzie grafu:");
    for(int row=0; row<V; ++row) 
    {
        for(int col =0; col<V; ++col)
            {
                        System.out.print((char)((int)'A'+row)+"-");
                        System.out.print((char)((int)'A'+col)+"-"+adj[row][col]+" ");
            }
        System.out.println();
    }
    System.out.println();
}
            
            
            
public void printGraphNum()
{
    System.out.println("Krawedzie grafu:");
    for(int row=0; row<V; ++row)
    {
        for(int col = 0; col <V; ++col)
        {
                System.out.print(row + "-");
                System.out.print(col + "-"+adj[row][col]+" ");
        }
        System.out.println();
     }
     System.out.println();
}

public void showMatrix()
{
    for(int i =0; i<V; i++)
    {    
        for(int j =0; j<V; j++)
            {
                System.out.print(adj[i][j]+" ");
            }
            System.out.println();
    }           
}

public int getUnVisitedVertex(int a, boolean[] visited)
{
    int max = adj[0][0];
    for(int i = 0; i<V; i++)
        for(int j = 0; j<V; j++)
            if(adj[i][j]>max) max = adj[i][j];
    int idx = 0;
    int min = max+1;
    for(int b=0; b<V; b++)
    {
        if(adj[a][b]!=0 && visited[b]==false && adj[a][b]<min)
        {
            min = adj[a][b];
            idx = b;
        }
    }
    if(min == max) return -1;
    else return idx;
}

public void checkit(int v)
{
    boolean[] visited = new boolean[V];
    for(int i = 0; i<V; i++)
    {
        visited[i] = false;
    }
    visited[v]=true;
    System.out.println(getUnVisitedVertex(v, visited));
}

public void nna(int src)
{
    LinkedList<Integer> object = new LinkedList<Integer>();
    boolean[] visited = new boolean[V];
    for(int i = 0; i<V; i++)
    {
        visited[i] = false;
    }
    int current = src;
    visited[current] = true;
    object.add(current);
    for(int i = 1; i<V; i++)
    {
        current = getUnVisitedVertex(current, visited);
        visited[current] = true;
        object.add(current);
    }
    System.out.println("Najkrotszy sasiad: "+object);
}




  public static void main(String[] args) throws Exception 
  {
      int V;
      V=5;
      near_neigh G=new near_neigh(V);
      G.addEdge(0,1,1);G.addEdge(0,2,3);G.addEdge(0,3,5);G.addEdge(0,4,2);
      G.addEdge(1,2,4);G.addEdge(1,3,6);G.addEdge(1,4,1);
      G.addEdge(2,3,2);G.addEdge(2,4,7);
      G.addEdge(3,4,3);
      G.printGraph();
      G.printGraphNum();
      G.showMatrix();
      G.nna(0);
  } 
}  
