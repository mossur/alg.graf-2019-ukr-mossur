import java.io.File;
import java.util.Stack;
import java.util.Queue;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Collections;


public class graph
{
    private int V;
    private int[][] adj;
    int inf = 1000000000;

    public graph(int V) 
     {
        if(V<0) throw new IllegalArgumentException("More than 0");
        this.V=V;
        this.adj=new int[V][V];
        for(int i=0; i<V; i++) 
        {
            for(int j=0; j<V; j++) 
            {
                this.adj[i][j]=0;
            }
        }
     }
    private void validateVertex(int v)
    {
        if(v<0 || v>=V) throw new IllegalArgumentException("Wierzcholek"+v+"nie jest w przedziale [0, "+ (V-1)+"]" );
    }
    
    public void addEdge(int v, int w, int l)
    {
     validateVertex(v); validateVertex(w);
     adj[v][w] = l; 
     adj[w][v]=l;
    }
   
   
public void printGraph()
{
    System.out.println("Krawedzie grafu:");
    for(int row=0; row<V; ++row) 
    {
        for(int col =0; col<V; ++col)
            {
                        System.out.print((char)((int)'A'+row)+"-");
                        System.out.print((char)((int)'A'+col)+"-"+adj[row][col]+" ");
            }
        System.out.println();
    }
    System.out.println();
}
            
            
            
public void printGraphNum()
{
    System.out.println("Krawedzie grafu:");
    for(int row=0; row<V; ++row)
    {
        for(int col = 0; col <V; ++col)
        {
                System.out.print(row + "-");
                System.out.print(col + "-"+adj[row][col]+" ");
        }
        System.out.println();
     }
     System.out.println();
}

public void showMatrix()
{
    for(int i =0; i<V; i++)
    {    
        for(int j =0; j<V; j++)
            {
                System.out.print(adj[i][j]+" ");
            }
            System.out.println();
    }           
}

public void Dijkstra(int src)
{
    int[] distance = new int[V];
    LinkedList<Integer> Q = new LinkedList<Integer>();
    Q.add(src);
    distance[src] = 0;
    for(int i = 0; i<V; i++)
    {
        if(i!=src)
        {
            distance[i] = inf;
            Q.add(i);
        }

    }
    distance[src] = 0;
    while(Q.size()!=0)
    {
        int min = inf;
        int idx = 0;
        for(int i = 0; i<V; i++)
        {
            if(Q.contains(i) && distance[i]<min)
                {
                    min = distance[i];
                    idx = i;
                } 
        }
        int v = idx;
        Q.remove(Q.indexOf(v));
        for(int u=0; u<V; u++)
        {
            if(distance[u]>distance[v]+adj[u][v])
            {
            distance[u]=distance[v]+adj[u][v];
            }
        }
    }
    System.out.println("Dejkstra distance: ");
    for(int i=0; i<V; i++)
        System.out.println(distance[i]+" ");
}



  public static void main(String[] args) throws Exception 
  {
      int V;
      V=5;
      graph G=new graph(V);
      G.addEdge(0,1,1);G.addEdge(0,2,3);G.addEdge(0,3,5);G.addEdge(0,4,2);
      G.addEdge(1,2,4);G.addEdge(1,3,6);G.addEdge(1,4,1);
      G.addEdge(2,3,2);G.addEdge(2,4,7);
      G.addEdge(3,4,3);
      G.printGraph();
      G.printGraphNum();
      G.showMatrix();
      G.Dijkstra(0);
  } 
}  
