import java.io.File;
import java.util.Stack;
import java.util.Queue;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Scanner;


public class fleuri 
{
    private int V;
    private boolean[][] adj;

    public fleuri(int V) 
     {
        if(V<0) throw new IllegalArgumentException("More than 0");
        this.V=V;
        this.adj=new boolean[V][V];
        for(int i=0; i<V; i++) 
        {
            for(int j=0; j<V; j++) 
            {
                this.adj[i][j]=false;
            }
        }
     }
    private void validateVertex(int v)
    {
        if(v<0 || v>=V) throw new IllegalArgumentException("Wierzcholek"+v+"nie jest w przedziale [0, "+ (V-1)+"]" );
    }
    
    public void addEdge(int v, int w)
    {
     validateVertex(v); validateVertex(w);
     adj[v][w] = true; 
     adj[w][v]=true;
    }
    public void removeEdge(int v, int w)
    {
        adj[v][w] = false;
        adj[w][v] = false;
    }
   
   
public void printGraph()
{
    System.out.println("Krawedzie grafu:");
    for(int row=0; row<V; ++row) 
    {
        for(int col =0; col<V; ++col)
            {
                if(adj[row][col])
                    {
                        System.out.print((char)((int)'A'+row)+"-");
                        System.out.print((char)((int)'A'+col)+" ");
                    }
            }
        System.out.println();
    }
    System.out.println();
}
            
            
            
public void printGraphNum()
{
    System.out.println("Krawedzie grafu:");
    for(int row=0; row<V; ++row)
    {
        for(int col = 0; col <V; ++col)
        {
            if(adj[row][col])
            {
                System.out.print(row + "-");
                System.out.print(col + " ");
            }
        }
        System.out.println();
     }
     System.out.println();
}

public void showMatrix()
{
    for(int i =0; i<V; i++)
    {    
        for(int j =0; j<V; j++)
            {
                System.out.print(adj[i][j]+" ");
            }
            System.out.println();
    }           
}
int getUnVisitedVertex(int a, boolean[] visited)
{
    for(int b=0; b<V; b=b+1)
    {
        if(adj[a][b]==true && visited[b]==false)return b;
    }
    return -1;
}

public void printEulerTour() 
{
    int u = 0;
    for (int row = 0; row < V; ++row)
    {
        int odd = 0;
        for (int col = 0; col < V; ++col)
        {
            if (adj[row][col] == true) odd++;
        }
        if (odd % 2 == 1)
        {
            u = row; break;
        }
    }
}


void printEulerUtil(int u)
{
    for (int v = 0; v < V; v++)
    {
        if (adj[u][v] == true && this.isValidNextEdge(u,v) == true)
        {
            System.out.println(u + "-" + v) ;
            this.removeEdge(u,v);
            this.printEulerUtil(v);
        }
    }
}

boolean isValidNextEdge(int u, int v)
{
    int count = 0;
    for (int i = 0; i < V; ++i)
    {
        if (adj[u][i] == true) count++;
    }
    if (adj[u][v] == true && count == 1) return true;
    int count1 = this.DFSCount(u);
    this.removeEdge(u,v);
    int count2 = this.DFSCount(u);
    this.addEdge(u,v);
    if (count1 > count2) return false;
    else return true;
}



int DFSCount(int a)
{
    boolean[] visited = new boolean[V];
    for (int k = 0; k < V; ++k) visited[k] = false;
    Stack<Integer> s = new Stack<Integer>();
    visited[a] = true;
    s.push(a);
    int count = 1;
    while (!s.empty())
    {
        int b = getUnVisitedVertex(s.peek(), visited);
        if (b == -1)
        {
            s.pop();
        }
        else
        {
           visited[b] = true;
           count++;
           s.push(b);
        }
     }
     return count;
}
    

  public static void main(String[] args) throws Exception 
  {
      int V;
      V=5;
      fleuri G=new fleuri(V);
      G.addEdge(0,1); G.addEdge(0,2); G.addEdge(0,3);
      G.addEdge(1,0); G.addEdge(1,2); G.addEdge(1,4);
      G.addEdge(2,0); G.addEdge(2,1);
      G.addEdge(3,0); G.addEdge(3,4);
      G.addEdge(4,1); G.addEdge(4,3);
      //G.printGraphNum();
      G.printEulerUtil(0);
      //G.printGraph();
      //G.showMatrix();
;
      
  } 
}  
